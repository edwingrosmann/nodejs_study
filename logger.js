const EventEmitter = require('events');
const uuid = require('uuid');

const level = { INFO: 'INFO', DEBUG: 'DEBUG' };

class Logger extends EventEmitter {
	info(msg) {
		this.emit('message', level.INFO, { id: uuid.v4(), msg: msg })
	}

	debug(msg) {
		this.emit('message', level.DEBUG, { id: uuid.v4(), msg: msg })
	}
}

module.exports = {
	register: (id) => {
		const logger = new Logger();
		logger.on('message', (level, data) => console.log(new Date().toISOString(), level, id, ':', data));
		return logger;
	},


};


