const http = require('http');
const Logger = require('./logger');

const logger = Logger.register('http');

function startHttp(port) {

	http.createServer((req, resp) => {
		resp.writeHead(200, { 'Content-Type': 'text/html' });
		resp.write('<h1>' + req.url + '</h1>');
		resp.end();
		req.url.substr(1).split('&').forEach((v) => { const s = v.split('='); logger.debug('Name=' + s[0] + ' Value=' + s[1]); });
		//		logger.log(req);
		//		logger.log(resp);
	}).listen(port);

	return 'Webserver Started on http://localhost:' + port;
}

module.exports = {
	startHttp: (port) => { return startHttp(port); }
}
